<?php

// Magic from https://stackoverflow.com/a/12941133/1816323
function is_regex(string $query):bool
{
    return (bool) var_export(@preg_match($query, null) !== false);
}
